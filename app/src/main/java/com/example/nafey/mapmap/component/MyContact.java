package com.example.nafey.mapmap.component;

public class MyContact implements Comparable<MyContact>{
    private String name;
    private String num;

    public MyContact(String name, String num) {
        this.name = name;
        this.num = num;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getNum() {
        return num;
    }

    @Override
    public String toString() {
        return this.getName() + " " + this.getNum();
    }

    @Override
    public int compareTo(MyContact another) {
        return this.name.compareTo(another.getName());
    }
}
