package com.example.nafey.mapmap.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.nafey.mapmap.component.DoubleText;
import com.example.nafey.mapmap.util.Handled;
import com.example.nafey.mapmap.util.MyContactsProvider;
import com.example.nafey.mapmap.util.MyHelper;
import com.example.nafey.mapmap.util.MyGetThread;
import com.example.nafey.mapmap.util.MyHandler;
import com.example.nafey.mapmap.util.MyLocationProvider;
import com.example.nafey.mapmap.util.MyPostThread;
import com.example.nafey.mapmap.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MapsActivity extends FragmentActivity implements
        SensorEventListener, Handled,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    private static final String TAG = "LOG__MapActivity";
    private static final String MY_NUM_KEY = "MY_NUM";
    private static final String URL_GET_SHARE = "http://naf.site11.com/getshare.php";
    private static final String URL_POST_LOC = "http://naf.site11.com/postloc.php";
    private static final String URL_POST_SHARE = "http://naf.site11.com/postshare.php";

    private static final int WELCOME_ACTIVITY_REQUEST_CODE = 1;
    private static final int CONTACTS_ACTIVITY_REQUEST_CODE = 2;

    private GoogleMap googleMap;
    private GoogleApiClient googleApiClient;
    private LatLng myLatLng;
    private LatLng clickedLatLng;
    private HashMap<String, LatLng> fetchedLatLng;
    private String myPhoneNum;

    private TextView txtPhone;
    private LinearLayout layContacts;

    private SensorManager mSensorManager;
    private Sensor accelerometer;
    private Sensor magnetometer;

    private float[] mGravity;
    private float[] mGeomagnetic;

    private void drawMarkers() {
        Log.d(TAG, "drawMarkers()");
        this.googleMap.clear();

        if (this.clickedLatLng != null) {
            this.googleMap.addMarker(new MarkerOptions().position(clickedLatLng));
        }

        if (this.fetchedLatLng != null) {
            for(String key : this.fetchedLatLng.keySet()) {
                LatLng l = this.fetchedLatLng.get(key);
                this.googleMap.addMarker(new MarkerOptions().position(l));
            }
        }

        if (this.myLatLng != null) {
            this.googleMap.addMarker(new MarkerOptions().position(this.myLatLng));
        }
    }

    public void processMarkerData(String content) {
        Log.d(TAG, "processMarkerData()");
        String num;
        Float lat;
        Float lng;
        try{
            Log.d(TAG, content);

            this.fetchedLatLng.clear();

            JSONArray jsonArray = new JSONArray(content);
            JSONObject jsonObject;


            for (int i = 0; i < jsonArray.length(); i++) {
                jsonObject = (JSONObject) jsonArray.get(i);
                num = (String) jsonObject.get("num");
                lat = Float.parseFloat((String) jsonObject.get("lat"));
                lng = Float.parseFloat((String) jsonObject.get("lng"));

                this.fetchedLatLng.put(num, new LatLng(lat, lng));
            }

            this.drawMarkers();

            for(String key : this.fetchedLatLng.keySet()) {
                DoubleText doubleText = new DoubleText(this);
                doubleText.setId(MyHelper.getIdFromNumber(Long.valueOf(key)));

                doubleText.setLeftText(key);

                this.layContacts.addView(doubleText);
            }
        }
        catch (JSONException ex) {
            Log.d(TAG, "JSON experienced an error");
            Log.d(TAG, ex.getMessage());
        }
    }

    public void getLocationFromOther(View view) {
        HashMap<String, String> get = new HashMap<>();
        get.put("tonum", this.myPhoneNum);
        new MyGetThread(get, MapsActivity.URL_GET_SHARE, new MyHandler(this)).start();
    }

    public void sendLocationToOther(View view) {
        Intent intent = new Intent(this, ContactsActivity.class);
        this.startActivityForResult(intent, MapsActivity.CONTACTS_ACTIVITY_REQUEST_CODE);
    }

    public void postLocation(View view) {
        Log.d(TAG, "postLocation()");
        String phoneNumber = this.txtPhone.getText().toString();

        HashMap<String, String> map = new HashMap<>();

        map.put("num", phoneNumber);
        map.put("lat", String.valueOf(this.myLatLng.latitude));
        map.put("lng", String.valueOf(this.myLatLng.longitude));

        //Post data
        new MyPostThread(map, MapsActivity.URL_POST_LOC).start();
    }

    public void handlerCallback(Message message) {
        Log.d(TAG, "handlerCallback()");
        this.processMarkerData(message.getData().getString("result"));
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult()");
        if (requestCode == WELCOME_ACTIVITY_REQUEST_CODE){
            if (resultCode == Activity.RESULT_OK) {
                String phone = data.getStringExtra("result");
                this.txtPhone.setText(phone);

                SharedPreferences prefs = this.getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor prefsEditor = prefs.edit();

                prefsEditor.putString(MY_NUM_KEY, phone);
                prefsEditor.apply();
            }
            else {
                Log.d(TAG, "The Activity finished unexpectedly");
            }
        } else if (requestCode == CONTACTS_ACTIVITY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                String toNum = data.getStringExtra("result");
                MyHelper.showStandardToast(this, "From " + this.myPhoneNum + ", To " + toNum);

                HashMap<String, String> map = new HashMap<>();
                map.put("to", MyHelper.formatNumber(toNum));
                map.put("from", MyHelper.formatNumber(this.myPhoneNum));
                String url = MapsActivity.URL_POST_SHARE;

                new MyPostThread(map, url).start();
            }
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float azimuth;
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) mGravity = event.values;
        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) mGeomagnetic = event.values;
        if (mGravity != null && mGeomagnetic != null) {
            float R[] = new float[9];
            float I[] = new float[9];
            //SensorManager.getRotationMatrix()
            boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
            if (success) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);
                azimuth = orientation[0]; // orientation contains: azimuth, pitch and roll
                azimuth = azimuth * 180f / 3.14f;

                for (String key : this.fetchedLatLng.keySet()) {
                    DoubleText doubleText = (DoubleText) findViewById(MyHelper.getIdFromNumber(Long.valueOf(key)));
                    doubleText.setRightText(String.valueOf(MyHelper.getBearing(this.myLatLng, this.fetchedLatLng.get(key)) - azimuth));
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        //setup variables
        this.txtPhone = (TextView) findViewById(R.id.txtPhone);
        this.layContacts = (LinearLayout) findViewById(R.id.layContact);

        this.fetchedLatLng = new HashMap<>();

        //Setup google map
        Log.d(TAG, "setting up map");
        if (googleMap == null) googleMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();

        googleMap.setTrafficEnabled(false);
        googleMap.setBuildingsEnabled(true);

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        this.googleApiClient.connect();

        this.googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                clickedLatLng = latLng;
                MapsActivity.this.drawMarkers();
            }
        });

        //Set my phone number
        SharedPreferences prefs = this.getPreferences(Context.MODE_PRIVATE); //private
        this.myPhoneNum = prefs.getString(MY_NUM_KEY, getResources().getString(R.string.default_phone_number));
        if (this.myPhoneNum.equals(this.getResources().getString(R.string.default_phone_number))) {
            Intent intent = new Intent(this, WelcomeActivity.class);
            this.startActivityForResult(intent, MapsActivity.WELCOME_ACTIVITY_REQUEST_CODE);
        } else  {
            this.txtPhone.setText(this.myPhoneNum);
        }

        this.getLocationFromOther(null);

        //Set up sensor
        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        for (int i = 0; i < 5; i++) {
            MyHelper.showStandardToast(this, MyContactsProvider.get().getContacts().get(i).getName());
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "onConnected()");
        Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (lastLocation != null) {
            this.myLatLng =  new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());

            //Zoom to my Location and add a marker
            CameraPosition cmp = new CameraPosition(this.myLatLng,15, 0, 0);
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cmp));

            //Update your location on startup
            this.postLocation(null);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
        mSensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    public void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed");
    }
}
