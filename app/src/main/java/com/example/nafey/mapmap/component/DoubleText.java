package com.example.nafey.mapmap.component;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.nafey.mapmap.R;

public class DoubleText extends LinearLayout {
    private TextView txtLeft;
    private TextView txtRight;

    public DoubleText(Context context) {
        super(context);
        LayoutInflater layoutInflater = LayoutInflater.from(this.getContext());
        layoutInflater.inflate(R.layout.double_text_view, this, true);

        this.txtLeft = (TextView) findViewById(R.id.txtLeft);
        this.txtRight = (TextView) findViewById(R.id.txtRight);

    }

    public void setLeftText(String str) {
        this.txtLeft.setText(str);
    }

    public void setRightText(String str) {
        this.txtRight.setText(str);
    }
}
