package com.example.nafey.mapmap.util;

import android.os.Message;

public interface Handled {
    void handlerCallback(Message message);
}
