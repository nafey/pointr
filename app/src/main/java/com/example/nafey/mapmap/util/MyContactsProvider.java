package com.example.nafey.mapmap.util;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;

import com.example.nafey.mapmap.activity.MyApplication;
import com.example.nafey.mapmap.component.MyContact;

import java.util.ArrayList;
import java.util.Collections;

public class MyContactsProvider {
    private static final String TAG = "LOG_MyContactsProvider";

    private static MyContactsProvider instance;
    private ArrayList<MyContact> contacts;

    public static final int STATUS_NEW = 0;
    public static final int STATUS_READY = 1;
    public static final int STATUS_FAILED = 2;

    private int status;

    private MyContactsProvider() {
        this.status = MyContactsProvider.STATUS_NEW;

        contacts = new ArrayList<>();

        ContentResolver cr = MyApplication.getAppContext().getContentResolver();

        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);

        try {
            if (cur.getCount() > 0) {
                while (cur.moveToNext()) {
                    String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                    String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    if (Integer.parseInt(cur.getString(
                            cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                        Cursor pCur = cr.query(
                                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
                                new String[]{id}, null);
                        while (pCur.moveToNext()) {
                            String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            contacts.add(new MyContact(name, phoneNo));
                        }
                        pCur.close();
                    }
                }
            }

            cur.close();

            Collections.sort(this.contacts);

            this.status = MyContactsProvider.STATUS_READY;
        } catch (Exception ex) {
            this.status = MyContactsProvider.STATUS_FAILED;

            Log.d(TAG, "Error while fetching contacts");
        }

    }

    public static MyContactsProvider get() {
        if (instance == null) {
            instance = new MyContactsProvider();
        }

        return instance;
    }

    public ArrayList<MyContact> getContacts() {
        return  contacts;
    }

    public int getStatus() {
        return  status;
    }
}
