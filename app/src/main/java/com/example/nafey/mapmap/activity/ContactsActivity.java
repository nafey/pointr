package com.example.nafey.mapmap.activity;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.nafey.mapmap.R;

import java.util.ArrayList;
import java.util.Collections;

public class ContactsActivity extends Activity {
    private static final String TAG = "LOG__ContactsActivity";

    private ArrayList<Contact> contacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate()");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);


        this.contacts = new ArrayList<>();
        ListView listView = (ListView) findViewById(R.id.listView);

        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        contacts.add(new Contact(name, phoneNo));
                    }
                    pCur.close();
                }
            }
        }

        cur.close();

        String[] values = new String[contacts.size()];
        for(int i = 0; i < contacts.size(); i++) {
            values[i] = this.contacts.get(i).toString();
        }

        final ArrayList<String> list = new ArrayList<>();
        Collections.addAll(list,values);

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Contact c = ContactsActivity.this.contacts.get(position);

                Intent intent = ContactsActivity.this.getIntent();
                intent.putExtra("result", c.getNum());
                ContactsActivity.this.setResult(Activity.RESULT_OK, intent);
                ContactsActivity.this.finish();
            }
        });
    }
    
    private class Contact{
        private String name;
        private String num;

        public void setName(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public String getNum() {
            return num;
        }

        public Contact(String name, String num) {
            this.name = name;
            this.num = num;
        }

        @Override
        public String toString() {
            return this.getName() + " " + this.getNum();
        }
    }
}
