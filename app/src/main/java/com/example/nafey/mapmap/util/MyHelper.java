package com.example.nafey.mapmap.util;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

public class MyHelper {
    private static final String TAG = "LOG__MyHelper";

    public static void showStandardToast(Activity activity, String message) {
        Context c = activity.getApplicationContext();
        Toast toast = Toast.makeText(c, message, Toast.LENGTH_SHORT);
        toast.show();
    }

    public static boolean isCorrectNumberFormat(Long l) {
        Log.d(TAG, "isCorrectNumberFormat()");
        if (l <= 999999999L) return false;
        return l <= 9999999999L;

    }

    public static boolean isCorrectNumberFormat(String str) {
        Log.d(TAG, "isCorrectNumberFormat()");

        if (null == str) return false;
        if ("".equals(str)) return false;

        str = formatNumber(str);

        try {
            Long l = Long.valueOf(str);
            return isCorrectNumberFormat(l);
        } catch (NumberFormatException ex) {
            Log.d(TAG, ex.getMessage());
            return false;
        }
    }

    public static String formatNumber(String str) {
        Log.d(TAG, "formatNumber()");

        if (str.length() >= 10) return str.substring(str.length() - 10, str.length());
        else return str;
    }

    public static int getIdFromNumber(Long num) {
        return (int)(num % 100000);
    }

    public static double getBearing(LatLng l1, LatLng l2) {
        double y = Math.sin(l2.longitude - l1.longitude) * Math.cos(l2.latitude);
        double x = Math.cos(l1.latitude) * Math.sin(l2.latitude) -
                Math.sin(l1.latitude) * Math.cos(l2.latitude) * Math.cos(l2.longitude - l1.longitude);

        double bearing = Math.toDegrees(Math.atan2(y, x));

        return bearing;
    }
}
