package com.example.nafey.mapmap.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Message;
import android.widget.TextView;

import com.example.nafey.mapmap.R;
import com.example.nafey.mapmap.util.Handled;
import com.example.nafey.mapmap.util.MyHandler;
import com.example.nafey.mapmap.util.MyInitThread;


public class SplashActivity extends Activity implements Handled {
    private TextView txtLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        this.txtLoading = (TextView) findViewById(R.id.txtLoadingProgress);
        (new MyInitThread(this, new MyHandler(this))).start();
    }

    public void setText(String str) {
        this.txtLoading.setText(str);
    }

    public void handlerCallback(Message message) {
        this.setText(message.getData().getString("result"));
    }
}
